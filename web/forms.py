from django import forms
from django.contrib.auth.models import User
from .models import Course, Person


class UserForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'email', 'username', 'password']


class CourseForm(forms.ModelForm):

    class Meta:
        model = Course
        fields = ['name', 'description', 'javascript_puzzle', 'thumbnail']


class PersonForm(forms.ModelForm):

    class Meta:
        model = Person
        fields = ['is_guide']
