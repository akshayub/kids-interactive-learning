from django.conf import settings
from django.conf.urls.static import static
from django.urls import path
from . import views

app_name = 'web'

urlpatterns = [
    path('login/', views.login_user, name='login'),
    path('register/', views.register, name='register'),
    path('logout/', views.logout_user, name='logout'),
    path('home/', views.home, name='home'),
    path('add_course/', views.add_course, name='add_course'),
    path('course_details/<int:cid>', views.view_course, name='view_course'),
    path('dashboard/<int:cid>', views.marks, name='marks'),
    path('dashboard/', views.dashboard, name='dashboard'),
    path('1/', views.testgame, name='testgame'),
    path('2/', views.mathgame, name='mathgame'),
    path('3/', views.shapegame, name='shapegame'),
    path('happy/<slug:user>/<int:happy>', views.happy, name='happy'),
    path('', views.index, name='index'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
