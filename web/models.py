from django.db import models
from django.contrib.auth.models import User


# Create your models here.
class Person(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    is_guide = models.BooleanField(default=False)

    def __str__(self):
        return self.user.first_name + " " + self.user.last_name


class Course(models.Model):
    name = models.CharField(max_length=50)
    description = models.TextField()
    added_on = models.DateField(auto_now_add=True)
    guide = models.ForeignKey(Person, on_delete=models.CASCADE)
    thumbnail = models.FileField(null=True)
    javascript_puzzle = models.FileField(null=True, blank=True)

    def __str__(self):
        return self.name


class AssignmentMarks(models.Model):
    student = models.ForeignKey(Person, on_delete=models.CASCADE)
    course = models.ForeignKey(Course, on_delete=models.CASCADE)
    marks = models.IntegerField(default=0)
    attempted_on = models.DateTimeField(auto_now_add=True)
    test_taken = models.BooleanField(default=False)
    happy = models.IntegerField(default=0)
    neutral = models.IntegerField(default=0)

    def __str__(self):
        return self.student.user.username + ' - ' + self.course.name
