from django.contrib.auth import authenticate, login, logout
from django.shortcuts import render, redirect
from django.http import HttpResponseForbidden, HttpResponse
from django.views.decorators.csrf import csrf_exempt

from web.forms import UserForm, PersonForm, CourseForm
from .models import Person, Course, AssignmentMarks, User


# Create your views here.
def index(request):
    if request.user.is_authenticated:
        return redirect('web:home')
    return render(request, 'web/index.html')


def login_user(request):
    if request.user.is_authenticated:
        return redirect('web:home')
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                return redirect('web:home')
            else:
                return render(request, 'web/login.html', {'error_message': 'Your account has not been activated!'})
        else:
            return render(request, 'web/login.html', {'error_message': 'Invalid login'})
    return render(request, 'web/login.html')


def logout_user(request):
    logout(request)
    return redirect('web:login')


def register(request):
    if request.user.is_authenticated:
        return redirect('web:home')
    uform = UserForm(request.POST or None)
    pform = PersonForm(request.POST or None)
    if uform.is_valid() and pform.is_valid():
        user = uform.save(commit=False)
        username = uform.cleaned_data['username']
        password = uform.cleaned_data['password']
        user.set_password(password)
        user.save()
        user = authenticate(username=username, password=password)
        person = pform.save(commit=False)
        person.user = user
        person.save()
        if user is not None:
            if user.is_active:
                login(request, user)
                if person.is_guide:
                    user.is_active = 0
                    user.save()
                    return render(
                        request,
                        'web/approval.html',
                        {'error_message': 'Your account will be activated after admin approval'}
                    )
                return redirect('web:home')

    context = {
        "uform": uform,
        "pform": pform
    }

    return render(request, 'web/register.html', context)


def home(request):
    if not request.user.is_authenticated:
        return redirect('web:login')
    person = Person.objects.get(user=request.user)
    if person.is_guide:
        courses = Course.objects.filter(guide=person)
        context = {
            'courses': courses
        }
        return render(request, 'web/g_home.html', context)
    else:
        courses = Course.objects.all()
        context = {
            'courses': courses
        }
        return render(request, 'web/s_home.html', context)


def add_course(request):
    if not request.user.is_authenticated:
        return redirect('web:login')
    else:
        person = Person.objects.get(user=request.user)
        if not person.is_guide:
            return HttpResponseForbidden()
        form = CourseForm(request.POST, request.FILES)
        if form.is_valid():
            course = form.save(commit=False)
            course.guide = person
            course.thumbnail = request.FILES['thumbnail']
            course.save()
            return redirect('web:home')
        form = CourseForm()
        context = {
            'form': form
        }
        return render(request, 'web/add_course.html', context)


def view_course(request, cid):
    if not request.user.is_authenticated:
        return redirect('web:login')
    else:
        person = Person.objects.get(user=request.user)
        course = Course.objects.get(pk=cid)
        url = request.build_absolute_uri()[:-len(request.get_full_path())] + '/' + str(course.id)
        print(url)
        context = {
            "course": course,
            'url': url
        }
        if not person.is_guide:
            return render(request, 'web/course_details.html', context)
        else:
            return render(request, 'web/g_course_details.html', context)


def dashboard(request):
    if not request.user.is_authenticated:
        return redirect('web:login')
    else:
        person = Person.objects.get(user=request.user)
        if not person.is_guide:
            return HttpResponseForbidden()
        courses = Course.objects.filter(guide=person)
        marklist = list()
        for course in courses:
            marks = list(AssignmentMarks.objects.filter(course=course).order_by('student__user__last_name'))
            marklist.append(marks)

        context = {
            'courses': courses,
            'markslist': marklist,
            'length': len(marklist)
        }
        return render(request, 'web/dashboard.html', context)


def testgame(request):
    if not request.user.is_authenticated:
        return redirect('web:login')
    if request.method == 'POST':
        user = request.POST['user']
        solution = request.POST['solution']
        a1 = request.POST['a1']
        a2 = request.POST['a2']
        a3 = request.POST['a3']
        a4 = request.POST['a4']
        a5 = request.POST['a5']

        person = Person.objects.get(user=user)
        if person.is_guide:
            return redirect('web:home')
        act_soln = [int(x) for x in list(solution)]
        ans = [int(a1), int(a2), int(a3), int(a4), int(a5)]

        score = 0
        for x in range(5):
            if act_soln[x] == ans[x]:
                score += 1
        course = Course.objects.get(pk=1)
        try:
            assignment = AssignmentMarks.objects.get(course=course, student=person)
            return HttpResponse("You already took this test! <a href=\"\">Click here</a> to go back")
        except Exception as e:
            assignment = AssignmentMarks(student=person, course=course, marks=score)
            assignment.save()

            return HttpResponse("Your test has been submitted! <a href=\"\">Click here</a> to go back")

    return render(request, 'web/testgame.html')


@csrf_exempt
def mathgame(request):
    if not request.user.is_authenticated:
        return redirect('web:login')
    if request.method == 'POST':
        marks = request.POST['marks']

        person = Person.objects.get(user=request.user)
        if person.is_guide:
            return HttpResponse("Testing...")
        else:
            course = Course.objects.get(pk=2)
            try:
                assignment = AssignmentMarks.objects.get(course=course, student=person)
                return HttpResponse("You already took this test!")
            except Exception as e:
                assignment = AssignmentMarks(student=person, course=course, marks=marks)
                assignment.save()
                data = "Your test has been submitted!"
                return HttpResponse(data)
    return render(request, 'web/mathgame.html')


def marks(request, cid):
    if not request.user.is_authenticated:
        return redirect('web:login')
    course = Course.objects.get(pk=cid)
    mark = list(AssignmentMarks.objects.filter(course=course).order_by('student__user__last_name'))

    context = {
        'course': course,
        'marks': mark
    }

    return render(request, 'web/marks.html', context)


@csrf_exempt
def shapegame(request):
    if not request.user.is_authenticated:
        return redirect('web:login')
    if request.method == 'POST':
        marks_obt = request.POST['resultstatus']
        act_marks = int(marks_obt.split('/')[0])
        # marks is a string of "student_marks/total_marks"

        person = Person.objects.get(user=request.user)
        if person.is_guide:
            return HttpResponse("Testing...")
        else:
            course = Course.objects.get(pk=3)
            assignment = AssignmentMarks.objects.get(course=course, student=person)
            assignment.test_taken = True
            assignment.marks = act_marks
            assignment.save()
            data = "Your test has been submitted!"
            return HttpResponse(data)
    person = Person.objects.get(user=request.user)
    course = Course.objects.get(pk=3)
    try:
        assignment = AssignmentMarks.objects.get(course=course, person=person)
    except Exception as e:
        assignment = AssignmentMarks(student=person, course=course, test_taken=False).save()
    return render(request, 'web/shapegame.html')


def happy(request, user, happy):
    try:
        u = User.objects.get(username=user)
        p = Person.objects.get(user=u)

        # Currently works only for Assignment 3
        try:
            assignment = AssignmentMarks.objects.get(student=p, course=3)
            if not assignment.test_taken:
                print("Entered deep!")
                assignment.happy += happy
                assignment.neutral += 1-happy
                assignment.save()
            return HttpResponse("OK")
        except Exception as e:
            print("Entered here...")
            return HttpResponse("OK")

    except Exception as e:
        print("Here!")
        return HttpResponse("OK")
